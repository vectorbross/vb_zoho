<?php

namespace Drupal\vb_zoho\Controller;

use Drupal\vb_zoho\ZohoApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * ZohoController.
 */
class ZohoController extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The logging channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The ACE API.
   *
   * @var \Drupal\ace_api\AceApi
   */
  protected $api;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, LoggerChannelFactoryInterface $logger_channel_factory, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->requestStack = $request_stack;
    $this->logger = $logger_channel_factory->get('zoho_api');
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->api = new ZohoApi();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  public function authorized() {
    $code = $this->requestStack->getCurrentRequest()->query->get('code'); // Grant code.
    $result = $this->api->getRefreshToken($code);
    $result = json_decode($result);
    // Store tokens.
    $this->configFactory->getEditable('vb_zoho.settings')
      ->set('api_grant', $code)
      ->set('api_refresh', $result->refresh_token)
      ->save();

    return [
      '#markup' => 'code: '.print_r($result,true),
    ];
  }
}
