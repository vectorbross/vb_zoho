<?php

namespace Drupal\vb_zoho\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vb_zoho\ZohoApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a config form for Zoho.
 */
class VBZohoConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_zoho_config';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_zoho.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_zoho.settings');
    $form['api_client'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('api_client'),
      '#required' => TRUE,
    ];
    $form['api_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('api_secret'),
      '#required' => TRUE,
    ];
    $form['api_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URI'),
      '#default_value' => $config->get('api_redirect'),
      '#required' => TRUE,
    ];
    $form['api_user'] = [
      '#type' => 'email',
      '#title' => $this->t('User email'),
      '#default_value' => $config->get('api_user'),
      '#required' => TRUE,
    ];
    $form['api_refresh'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Refresh token'),
      '#default_value' => $config->get('api_refresh'),
      '#disabled' => TRUE,
    ];
    $form['api_grant'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Grant token'),
      '#default_value' => $config->get('api_grant'),
      '#disabled' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->configFactory->getEditable('vb_zoho.settings');
    $config
      ->set('api_client', $form_state->getValue('api_client'))
      ->set('api_secret', $form_state->getValue('api_secret'))
      ->set('api_redirect', $form_state->getValue('api_redirect'))
      ->set('api_user', $form_state->getValue('api_user'))
      ->save();
    $api = new ZohoApi();
    $api->authorize();
  }

}
