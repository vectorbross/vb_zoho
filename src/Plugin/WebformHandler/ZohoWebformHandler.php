<?php

namespace Drupal\vb_zoho\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\vb_zoho\ZohoApi;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Submits .
 *
 * @WebformHandler(
 *   id = "zoho_webform_handler",
 *   label = @Translation("Zoho Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to Zoho."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ZohoWebformHandler extends WebformHandlerBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_key' => '',
      'email' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['list_key'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#empty_option' => $this->t('- Select a List -'),
      '#default_value' => $this->configuration['list_key'],
      '#options' => $this->getLists(),
      '#description' => $this->t('Select the list you want to send this submission to.'),
    ];

    // Get webform elements as base for mapping.
    $elements = $this->webform->getElementsDecodedAndFlattened();
    foreach($elements as $key => $element) {
      if(!in_array($element['#type'], ['textfield', 'email'])) {
        unset($elements[$key]);
      }
    }
    $options = [];
    foreach($elements as $key => $element) {
      $options[$key] = $element['#title'];
    }

    $api = new ZohoApi();
    foreach($api->getFields() as $fieldname) {
      $form[$fieldname->FIELD_NAME] = [
        '#type' => 'select',
        '#title' => $this->t($fieldname->DISPLAY_NAME),
        '#options' => $options,
        '#empty_option' => $this->t('- Select a field -'),
        '#required' => $fieldname->IS_MANDATORY == '1',
        '#default_value' => $this->configuration[$fieldname->FIELD_NAME],
      ];
    }

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['list_key'] = $form_state->getValue('list_key');
    $api = new ZohoApi();
    foreach($api->getFields() as $fieldname) {
      $this->configuration[$fieldname->FIELD_NAME] = $form_state->getValue($fieldname->FIELD_NAME);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    // Get the submission data.
    $values = $webform_submission->getData();

    $data = [];

    // Post to Zoho
    $api = new ZohoApi();
    foreach($api->getFields() as $fieldname) {
      if(isset($values[$this->configuration[$fieldname->FIELD_NAME]])) {
        $data[$fieldname->DISPLAY_NAME] = $values[$this->configuration[$fieldname->FIELD_NAME]];
      }
    }
    if(count($data)) {
      $api->listSubscribe($this->configuration['list_key'], $data);
    }
  }

  /**
   * Get Mailchimp lists.
   */
  protected function getLists() {
    $lists = [];

    $api = new ZohoApi();
    $mailing_lists = $api->getMailingLists();

    foreach ($mailing_lists as $list) {
      $lists[$list->listkey] = $list->listname;
    }

    return $lists;
  }

}
