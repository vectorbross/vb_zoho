<?php

namespace Drupal\vb_zoho;

use Drupal\Core\Routing\TrustedRedirectResponse;
use GuzzleHttp\Client;

class ZohoApi {
  protected $config;

  protected $client;

  protected $logger;

  public function __construct() {
    $this->config = \Drupal::config('vb_zoho.settings');
    $this->client = new Client();
    $this->logger = \Drupal::logger('zoho_api');
  }

  /**
   * ACE API client request.
   *
   * @param $method
   * @param $resource
   * @param $params
   * @return false|string
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function request($method, $resource, $params, $content_type = 'application/json') {
    $data = [
      'headers' => [
        'Content-Type' => $content_type,
        'Authorization' => 'Zoho-oauthtoken ' . $this->getAccessToken(),
      ],
      'http_errors' => FALSE,
    ];
    if($method == 'GET') {
      if(count($params)) {
        $resource .= '?'. http_build_query($params);
      }
    } else {
      switch($content_type) {
        case 'application/json':
          $data['json'] = $params;
        break;
        case 'application/x-www-form-urlencoded':
          $data['form_params'] = $params;
        break;
      }
    }
    $response = $this->client->request($method, 'https://campaigns.zoho.com' . $resource, $data);

    if($response->getStatusCode() == 200) {
      $results = json_decode($response->getBody()->getContents());
      if(property_exists($results, 'status') && $results->status == 'error') {
        $this->logger->notice('The resource @resource returns an error message: @message', ['@resource' => $resource, '@message' => $results->message]);
        return FALSE;
      }
      return json_decode($results);
    }
    else {
      $this->logger->notice('The resource @resource returns an HTTP error (@error). Reponse: @body', ['@resource' => $resource, '@error' => $response->getStatusCode(), '@body' => (string) $response->getBody()]);
      return FALSE;
    }
  }

  public function authorize() {
    $response = new TrustedRedirectResponse($this->getAuthorizationUrl());
    $response->send();
  }

  /**
   * Build authorization URL.
   *
   * @return string
   *   Full authorization URL.
   */
  public function getAuthorizationUrl() {
    $params = [
      'prompt' => 'consent',
      'scope' => 'ZohoCampaigns.contact.ALL',
      'client_id' => $this->config->get('api_client'),
      'response_type' => 'code',
      'access_type' => 'offline',
      'redirect_uri' => $this->config->get('api_redirect'),
    ];
    $query_string = http_build_query($params);

    return 'https://accounts.zoho.com/oauth/v2/auth?' . $query_string;
  }

  public function getRefreshToken($code) {
    $response = $this->client->request('POST', 'https://accounts.zoho.com/oauth/v2/token', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => [
        'client_id' => $this->config->get('api_client'),
        'grant_type' => 'authorization_code',
        'client_secret' => $this->config->get('api_secret'),
        'redirect_uri' => $this->config->get('api_redirect'),
        'code' => $code, // $code is grant token
      ],
      'http_errors' => FALSE,
    ]);
    if($response->getStatusCode() == 200) {
      $body = (string) $response->getBody();
      return $body;
    }
    return FALSE;
  }

  public function getAccessToken() {
    $response = $this->client->request('POST', 'https://accounts.zoho.com/oauth/v2/token', [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'form_params' => [
        'client_id' => $this->config->get('api_client'),
        'grant_type' => 'refresh_token',
        'client_secret' => $this->config->get('api_secret'),
        'refresh_token' => $this->config->get('api_refresh'), // $code is grant token
      ],
      'http_errors' => FALSE,
    ]);
    if($response->getStatusCode() == 200) {
      $body = (string) $response->getBody();
      $data = json_decode($body);
      return $data->access_token;
    }
    return FALSE;
  }

  /**
   * Get the available mailing lists.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getFields() {
    $params = [
      'type' => 'json',
    ];
    $response = $this->request('GET', '/api/v1.1/contact/allfields', $params, 'application/x-www-form-urlencoded');

    if (isset($response->response->fieldnames->fieldname)) {
      return $response->response->fieldnames->fieldname;
    }
    return [];
  }

  /**
   * Get the available mailing lists.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getMailingLists() {
    $params = [
      'resfmt' => 'JSON',
    ];
    $lists = $this->request('GET', '/api/v1.1/getmailinglists', $params);

    if (isset($lists->list_of_details)) {
      return $lists->list_of_details;
    }
    return [];
  }

  /**
   * Subscribes a new contact to a list.
   *
   * @param $list_key
   * @param $contact_data
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function listSubscribe($list_key, $contact_data) {
    $params = [
      'listkey' => $list_key, // The list-ide we get from getMailinglists and set in webformhandler.
      'resfmt' => 'JSON',
      'contactinfo' => json_encode($contact_data),
    ];
    $response = $this->request('POST', '/api/v1.1/json/listsubscribe', $params, 'application/x-www-form-urlencoded');
  }

}
